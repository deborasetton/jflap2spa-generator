require 'matrix'
require './code_generator'

module Syntactic
  
  class OutputBuilder
    
    #
    # Inicialização
    #
    def initialize(scraper_obj)
      @result = scraper_obj
    end

    #
    # Geração do código de saída.
    #
    def generate_output

      output = File.open("syntactic_generated_blank.c", 'r+') {|f| f.read}
      code_generator = Syntactic::CodeGenerator.new
    
      output.gsub!(/#IS_FINAL_ARRAY#/, code_generator.g_is_final_array(@result.num_states, @result.final_states))
      output.gsub!(/#RESERVED_WORDS_MAP#/, code_generator.g_reserved_words_map(@result.columns))
      output.gsub!(/#LINE_COUNT#/, @result.num_states.to_s)
      output.gsub!(/#COLUMN_COUNT#/, @result.columns.keys.count.to_s)
      output.gsub!(/#STATE_MACHINE_ASSIGNMENTS#/, code_generator.g_transitions(@result))
      output.gsub!(/#GET_COLUMN_BODY#/, code_generator.g_get_column_fn(@result.columns))
      
      File.open("syntactic_generated.c", 'w') {|f| f.write(output)}
      
    end
    
  end
  
end