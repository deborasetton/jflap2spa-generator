module Syntactic
  
  class CodeGenerator
    
    # Caracteres especiais.
    SPECIAL_CHARACTERS = [
      "!", "%", "&", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "[", "]", "^", "|",
      "=>", "/=", "+=", "-=", "*=", "%=", "++", "--", "==", "!=", ">=", "<=" ]

    # Palavras reservadas, na ordem em que elas aparecem no lex.c.
    RESERVED_WORDS = [
      "int", "float", "bool", "char", "void", "rec", "struct", "end", "if", "else", "match", "with", "while", "repeat", 
      "until", "for", "in", "to", "break", "continue", "enum", "read", "print", "true", "false", "def", "proc",
      "string", "scope" ]

    # Como acessar a matriz de transições.
    STATE_MACHINE_PREFIX = "machine->transitionMatrix"
    
    MATRIX_ROWS = 200
    MATRIX_COLS = 200
    
    #
    # Gera a declaração do vetor de estados finais.
    #
    def g_is_final_array(num_states, final_states)

      final_states_array = Array.new(num_states) {0}
      final_states.each {|index| final_states_array[index] = 1}
      
      str = "/* Final states: #{final_states.join(', ')} */\nint _isFinal[#{num_states}] = {#{final_states_array.join(', ')}}"
    end
    
    #
    # Gera os comandos de atribuição para preencher a matriz de transições.
    #
    def g_transitions(result)

      #
      # PASSO 1: Construção da representação interna da matriz de transições
      #
      
      matrix = Array.new(MATRIX_ROWS) { Array.new(MATRIX_COLS) }

      #
      # PASSO 1.1: Para cada estado final, existe uma transição, com qualquer terminal, de desempilhamento.
      #
      
      result.final_states.each do |final_state|
        result.columns.each do |key, column|       
          matrix[final_state][column] = {:next_state => -1, :push => -1, :semantic_action => "&final_semantic_tbd"}
        end
      end
      
      #
      # PASSO 1.2: Geração dos resto das transições (as que não são com estados finais).
      #
      
      result.transitions.each do |transition|
      
        if transition[:terminal]
          # Transição com terminal; é mais simples.
          
          matrix[transition[:from]][result.columns[transition[:column]]] = {
            :next_state => transition[:to], 
            :push => -1, 
            :semantic_action => "&semantic_tbd", 
            :col_number => result.columns[transition[:column]], 
            :terminal => transition[:terminal]
          }
          
        else
          # Transição com não terminal; gera várias transições, uma com cada terminal.

          result.columns.count.times do |column|
            matrix[transition[:from]][column] = {
              :next_state => result.sub_machines_offsets[transition[:column]], 
              :push => transition[:to], 
              :semantic_action => "&semantic_tbd", 
              :col_number => column, 
              :terminal => transition[:terminal]}
          end
        end  
      end
      
      #
      # PASSO 2: Geração da saída (armazenada na variável output)
      #
      
      # Lista que de hashes. Cada hash é relativo a um estado, e contém uma lista com as colunas de transição
      # com terminais. Essa lista é usada para gerar a condição do IF na saída.
      different_columns_list = []
      
      output = ""

      (0..MATRIX_ROWS-1).each do |i|
        
        #
        # PASSO 2.1: Para cada linha, é feita uma contagem das transições que mais aparecem, para que as transições
        # com não-terminais, que normalmente aparecem muitas vezes, sejam geradas em um loop, ao invés de gerar
        # uma transição por vez.
        #
        
        # Copia a linha atual, eliminando todos os nils que aparecem (atenção: isso faz com que a informação sobre
        # o número da coluna desapareça)
        line = matrix[i].clone.compact

        # Construção de um hash com Key = Número do próximo estado e Value = Total de vezes que esse estado aparece.
        freq = line.inject(Hash.new(0)) { |hsh, value| hsh[value[:next_state]] += 1; hsh }
        
        # Ordenação das transições por frequência
        line = line.sort{ |a, b| freq[b[:next_state]] <=> freq[a[:next_state]] }
        
        # Remove da linha (que é uma lista de transições) todas as transições repetidas. 
        # Para os não-terminais, uma transição é considerada repetida se ela leva ao mesmo estado (:next_state).
        # Para os terminais, é preciso levar em conta o número da coluna, por isso o if.
        line = line.inject([]) do |list, value| 
          
          # Tenta achar a transição na lista
          already_in_the_list = false
          list.each { |v| already_in_the_list = true if ((v[:next_state] == value[:next_state]) && !v[:terminal]) }
          
          # Adiciona se não encontrou e continua
          list << value unless already_in_the_list
          list
        end 
  
        # Esse if testa se a transição mais frequente pode ser gerada usando um FOR. 
        # Para que possa, a transição tem que ser com não-terminais (ou seja, :push != -1) ou
        # tem que ser uma transição de desempilhamento (identificada pela ação semântica)
        if line.count > 0 && ((line[0][:push] != -1) || (line[0][:semantic_action] == "&final_semantic_tbd"))
          
          # Obtém a lista com as colunas que não devem entrar no FOR e armazena na lista mais geral
          different_columns = line[1..-1].map{|v| v[:col_number]}
          different_columns_list << {:state => i, :different_columns => different_columns, :elem => line[0]}
          
          # Para todas as transições que não estarão cobertas pelo FOR, deve ser gerada uma linha na saída.
          line[1..-1].each do |transition|
            output += g_transition(result, transition, i, transition[:col_number])
          end
          
        else
          # Não há transições com não-terminais nessa linha.
          
          (0..MATRIX_COLS-1).each do |j|
            if (elem = matrix[i][j])
              output += g_transition(result, elem, i, j)
            end
          end
        end
      end # loop i
      
      #
      # PASSO 2.1: Geração dos FORs.
      #

      output += "    for(i = 0; i < #{result.columns.keys.count}; i++)\n    {\n"
      
      different_columns_list = different_columns_list.sort_by{ |v| v[:different_columns].count }
      
      different_columns_list.each do |value|
        
        comm_info = g_comment_information(result, value[:state], value[:elem])
        
        if comm_info[:push_submachine_name]
          output += g_comment(8, comm_info, :non_terminal)
        else
          output += g_comment(8, comm_info, :final_state)
        end
        
        if value[:different_columns].any?
          output += "#{' '*8}if (i != #{value[:different_columns].join(" && i != ")})\n"
          output += g_stmt(12, value[:state], value[:elem])
        else
           output += g_stmt(8, value[:state], value[:elem])
        end
        
      end
      
      output += "#{' '*4}}\n"

      return output

    end
    
    #
    # Gera o código para a a função GetColumn.
    #
    def g_get_column_fn(spa_columns)
  
      switch_str = <<SWITCH
      switch (token.Class)
      {
          case CharConstant:
              return #{spa_columns['constante_char']};
              break;
          case StringConstant:
              return #{spa_columns['constante_string']};
              break;
          case IntegerConstant:
              return #{spa_columns['numero_inteiro']};
              break;
          case FloatConstant:
              return #{spa_columns['numero_float']};
              break;
          case Identifier:
              return #{spa_columns['identificador']};
              break;
          case SpecialCharacter:
              switch (token.Value)
              {
SPECIAL_CHARACTERS_CASES
              }
              break;
          case ReservedWord:
              return _reservedWordMap[token.Value];
              break;
          case EoF:
              return #{spa_columns['eos']};
              break;
          default:
              return -1;
              break;

      }
SWITCH
  
      special_characters_cases = ""

      SPECIAL_CHARACTERS.each do |special_char|

        hex_str = get_hex_hash_value(special_char)

        special_characters_cases += <<STR
                case #{hex_str}: // #{special_char}
                    return #{spa_columns[special_char] || -1};
                    break;
STR
    
    
      end
  
      switch_str.gsub!(/SPECIAL_CHARACTERS_CASES/, special_characters_cases)
      
      return switch_str
    end
    
    #
    # Gera o array que mapeia palavras reservadas para o número da coluna
    #
    def g_reserved_words_map(columns)
      
      reserved_words_array = Array.new(RESERVED_WORDS.count)
      
      RESERVED_WORDS.each_with_index do |word, index|
        reserved_words_array[index] = columns[word] ? "#{columns[word]} /*#{word}*/" : "-1 /*#{word}*/"
      end

      str = "int _reservedWordsMap[#{RESERVED_WORDS.count}] = {#{reserved_words_array.join(", ")}}"
      
    end
    
    
    #
    # PRIVATE METHODS.
    #
    
    private
    
    #
    # Gera um statement de atribuição (uma transição da matriz).
    #
    def g_stmt(spaces_num, state, elem, i=nil)
      "#{' '*spaces_num}machine->transitionMatrix[nSPAColumns * #{state} + #{i ? i : 'i'}] = {#{elem[:next_state]}, #{elem[:push]}, #{elem[:semantic_action]}};\n\n"
    end
    
    #
    # Gera informação para ser usada em comentários.
    #
    def g_comment_information(result, state_number, elem, col_number=0)
      {
        :from_submachine_name => result.states[state_number],
        :from_submachine_state => (result.states[state_number] ? 
          (state_number - result.sub_machines_offsets[result.states[state_number]]) : 
          :error),
        :to_submachine_name => result.states[elem[:next_state]],
        :to_submachine_state => (result.states[elem[:next_state]] ?
          (elem[:next_state] - result.sub_machines_offsets[result.states[elem[:next_state]]]) :
          :error),
        :push_submachine_name => result.states[elem[:push]],
        :push_submachine_state => (result.states[elem[:push]] ? 
          (elem[:push] - result.sub_machines_offsets[result.states[elem[:push]]]) : 
          :error),
        :with_column => result.columns_by_index[col_number]
      }
    end
    
    #
    # Gera um comentário de uma transição.
    #
    def g_comment(spaces_num, comm_info, type)
      
      comm = ""
      
      if type == :final_state
        comm +=  "#{' '*spaces_num}/* ({#{comm_info[:from_submachine_name]}, #{comm_info[:from_submachine_state]}}) -> DESEMPILHA */\n"
      elsif type == :non_terminal
        comm +=  "#{' '*spaces_num}/* ({#{comm_info[:from_submachine_name]}, #{comm_info[:from_submachine_state]}}) -> {#{comm_info[:to_submachine_name]}, #{comm_info[:to_submachine_state]}}, VOLTA PARA {#{comm_info[:push_submachine_name]}, #{comm_info[:push_submachine_state]}} */\n"
      elsif type == :normal
        comm +=  "#{' '*spaces_num}/* ({#{comm_info[:from_submachine_name]}, #{comm_info[:from_submachine_state]}}  #{comm_info[:with_column]}) -> (#{comm_info[:to_submachine_name]}, #{comm_info[:to_submachine_state]}}, NAO EMPILHA */\n"
      else 
        raise 'Unknown comment type'
      end
      
      return comm
    end
    
    
    #
    # Gera uma transição da matriz (comentário + código)
    # 
    def g_transition(result, elem, i, j)
      
      output = ""
      
      comm_info = g_comment_information(result, i, elem, j)
      
      if comm_info[:to_submachine_name]
        output += g_comment(4, comm_info, :normal)
      else
        output += g_comment(4, comm_info, :final_state)
      end
      
      output += g_stmt(4, i, elem, j)  
      return output
    end

    
    #
    # Método auxiliar, retorna a string que representa o número hexadecimal que representa um caractere especial.
    #
    def get_hex_hash_value(special_char)

      return "0x#{special_char.ord.to_s(16)}" if special_char.length == 1

      least_significant = special_char[0].ord
      most_significant = special_char[1].ord

      hash_value = least_significant + (most_significant << 8)

      return "0x#{hash_value.to_s(16).upcase}"
      
    end

  end
  
end







