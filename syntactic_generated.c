/*******************************************************
    ATENÇÃO: ESTE ARQUIVO FOI GERADO AUTOMATICAMENTE.
*******************************************************/
    
#include "syntactic_generated.h"
#include "spstatemachine.h"
#include "semantic.h"

/* Final states: 5, 40, 45, 83, 86, 88, 95 */
int _isFinal[99] = {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0}

int _reservedWordsMap[29] = {6 /*int*/, 7 /*float*/, 8 /*bool*/, 9 /*char*/, -1 /*void*/, 14 /*rec*/, 4 /*struct*/, 0 /*end*/, 20 /*if*/, 44 /*else*/, 21 /*match*/, 43 /*with*/, 22 /*while*/, 23 /*repeat*/, 46 /*until*/, 24 /*for*/, 45 /*in*/, 42 /*to*/, 29 /*break*/, 30 /*continue*/, 3 /*enum*/, 25 /*read*/, 26 /*print*/, 55 /*true*/, 56 /*false*/, 1 /*def*/, 2 /*proc*/, 10 /*string*/, 27 /*scope*/}

int IsFinal(int state)
{
    return _isFinal[state];
}

int GetColumn(Token token)
{
  
      switch (token.Class)
      {
          case CharConstant:
              return 32;
              break;
          case StringConstant:
              return 31;
              break;
          case IntegerConstant:
              return 19;
              break;
          case FloatConstant:
              return 53;
              break;
          case Identifier:
              return 11;
              break;
          case SpecialCharacter:
              switch (token.Value)
              {
                case 0x21: // !
                    return 54;
                    break;
                case 0x25: // %
                    return 51;
                    break;
                case 0x26: // &
                    return 59;
                    break;
                case 0x28: // (
                    return 15;
                    break;
                case 0x29: // )
                    return 12;
                    break;
                case 0x2a: // *
                    return 49;
                    break;
                case 0x2b: // +
                    return 52;
                    break;
                case 0x2c: // ,
                    return 17;
                    break;
                case 0x2d: // -
                    return 47;
                    break;
                case 0x2e: // .
                    return 34;
                    break;
                case 0x2f: // /
                    return 50;
                    break;
                case 0x3a: // :
                    return 40;
                    break;
                case 0x3b: // ;
                    return 13;
                    break;
                case 0x3c: // <
                    return 64;
                    break;
                case 0x3d: // =
                    return 33;
                    break;
                case 0x3e: // >
                    return 62;
                    break;
                case 0x3f: // ?
                    return -1;
                    break;
                case 0x5b: // [
                    return 16;
                    break;
                case 0x5d: // ]
                    return 18;
                    break;
                case 0x5e: // ^
                    return 48;
                    break;
                case 0x7c: // |
                    return 41;
                    break;
                case 0x3E3D: // =>
                    return 28;
                    break;
                case 0x3D2F: // /=
                    return 35;
                    break;
                case 0x3D2B: // +=
                    return 36;
                    break;
                case 0x3D2D: // -=
                    return 37;
                    break;
                case 0x3D2A: // *=
                    return 38;
                    break;
                case 0x3D25: // %=
                    return 39;
                    break;
                case 0x2B2B: // ++
                    return 60;
                    break;
                case 0x2D2D: // --
                    return 61;
                    break;
                case 0x3D3D: // ==
                    return 57;
                    break;
                case 0x3D21: // !=
                    return 58;
                    break;
                case 0x3D3E: // >=
                    return 63;
                    break;
                case 0x3D3C: // <=
                    return 65;
                    break;

              }
              break;
          case ReservedWord:
              return _reservedWordMap[token.Value];
              break;
          case EoF:
              return ;
              break;
          default:
              return -1;
              break;

      }

  
}

SPStateMachine * InitializeSPA()
{
    SPStateMachine *machine;   
    int nSPAColumns = 66;
    SPTransition null_transition = {-1, -1, NULL};

    machine = SPCreateStateMachine(99, 66, null_transition);

    /* SPA transitions */

    /* ({programa, 0}  eof) -> (programa, 5}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 0 + 5] = {5, -1, &semantic_tbd};

    /* ({programa, 0}  struct) -> (programa, 4}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 0 + 4] = {4, -1, &semantic_tbd};

    /* ({programa, 0}  enum) -> (programa, 3}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 0 + 3] = {3, -1, &semantic_tbd};

    /* ({programa, 0}  proc) -> (programa, 2}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 0 + 2] = {2, -1, &semantic_tbd};

    /* ({programa, 0}  def) -> (programa, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 0 + 1] = {1, -1, &semantic_tbd};

    /* ({programa, 1}  int) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 1 + 6] = {7, -1, &semantic_tbd};

    /* ({programa, 1}  float) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 1 + 7] = {7, -1, &semantic_tbd};

    /* ({programa, 1}  bool) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 1 + 8] = {7, -1, &semantic_tbd};

    /* ({programa, 1}  char) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 1 + 9] = {7, -1, &semantic_tbd};

    /* ({programa, 1}  string) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 1 + 10] = {7, -1, &semantic_tbd};

    /* ({programa, 1}  identificador) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 1 + 11] = {7, -1, &semantic_tbd};

    /* ({programa, 1}  rec) -> (programa, 6}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 1 + 14] = {6, -1, &semantic_tbd};

    /* ({programa, 2}  identificador) -> (programa, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 2 + 11] = {8, -1, &semantic_tbd};

    /* ({programa, 2}  rec) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 2 + 14] = {7, -1, &semantic_tbd};

    /* ({programa, 3}  identificador) -> (programa, 23}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 3 + 11] = {23, -1, &semantic_tbd};

    /* ({programa, 4}  identificador) -> (programa, 10}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 4 + 11] = {10, -1, &semantic_tbd};

    /* ({programa, 4}  rec) -> (programa, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 4 + 14] = {9, -1, &semantic_tbd};

    /* ({programa, 6}  int) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 6 + 6] = {7, -1, &semantic_tbd};

    /* ({programa, 6}  float) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 6 + 7] = {7, -1, &semantic_tbd};

    /* ({programa, 6}  bool) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 6 + 8] = {7, -1, &semantic_tbd};

    /* ({programa, 6}  char) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 6 + 9] = {7, -1, &semantic_tbd};

    /* ({programa, 6}  string) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 6 + 10] = {7, -1, &semantic_tbd};

    /* ({programa, 6}  identificador) -> (programa, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 6 + 11] = {7, -1, &semantic_tbd};

    /* ({programa, 7}  identificador) -> (programa, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 7 + 11] = {8, -1, &semantic_tbd};

    /* ({programa, 8}  () -> (programa, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 8 + 15] = {11, -1, &semantic_tbd};

    /* ({programa, 9}  identificador) -> (programa, 10}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 9 + 11] = {10, -1, &semantic_tbd};

    /* ({programa, 10}  ;) -> (programa, 12}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 10 + 13] = {12, -1, &semantic_tbd};

    /* ({programa, 11}  int) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 11 + 6] = {13, -1, &semantic_tbd};

    /* ({programa, 11}  float) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 11 + 7] = {13, -1, &semantic_tbd};

    /* ({programa, 11}  bool) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 11 + 8] = {13, -1, &semantic_tbd};

    /* ({programa, 11}  char) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 11 + 9] = {13, -1, &semantic_tbd};

    /* ({programa, 11}  string) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 11 + 10] = {13, -1, &semantic_tbd};

    /* ({programa, 11}  identificador) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 11 + 11] = {13, -1, &semantic_tbd};

    /* ({programa, 11}  )) -> (programa, 14}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 11 + 12] = {14, -1, &semantic_tbd};

    /* ({programa, 12}  int) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 12 + 6] = {15, -1, &semantic_tbd};

    /* ({programa, 12}  float) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 12 + 7] = {15, -1, &semantic_tbd};

    /* ({programa, 12}  bool) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 12 + 8] = {15, -1, &semantic_tbd};

    /* ({programa, 12}  char) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 12 + 9] = {15, -1, &semantic_tbd};

    /* ({programa, 12}  string) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 12 + 10] = {15, -1, &semantic_tbd};

    /* ({programa, 12}  identificador) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 12 + 11] = {15, -1, &semantic_tbd};

    /* ({programa, 13}  identificador) -> (programa, 20}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 13 + 11] = {20, -1, &semantic_tbd};

    /* ({programa, 14}  ;) -> (programa, 16}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 14 + 13] = {16, -1, &semantic_tbd};

    /* ({programa, 15}  identificador) -> (programa, 17}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 15 + 11] = {17, -1, &semantic_tbd};

    /* ({programa, 16}  end) -> (programa, 18}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 16 + 0] = {18, -1, &semantic_tbd};

    /* ({programa, 17}  ;) -> (programa, 19}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 17 + 13] = {19, -1, &semantic_tbd};

    /* ({programa, 18}  ;) -> (programa, 0}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 18 + 13] = {0, -1, &semantic_tbd};

    /* ({programa, 19}  end) -> (programa, 18}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 19 + 0] = {18, -1, &semantic_tbd};

    /* ({programa, 19}  int) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 19 + 6] = {15, -1, &semantic_tbd};

    /* ({programa, 19}  float) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 19 + 7] = {15, -1, &semantic_tbd};

    /* ({programa, 19}  bool) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 19 + 8] = {15, -1, &semantic_tbd};

    /* ({programa, 19}  char) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 19 + 9] = {15, -1, &semantic_tbd};

    /* ({programa, 19}  string) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 19 + 10] = {15, -1, &semantic_tbd};

    /* ({programa, 19}  identificador) -> (programa, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 19 + 11] = {15, -1, &semantic_tbd};

    /* ({programa, 20}  )) -> (programa, 14}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 20 + 12] = {14, -1, &semantic_tbd};

    /* ({programa, 20}  [) -> (programa, 21}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 20 + 16] = {21, -1, &semantic_tbd};

    /* ({programa, 20}  ,) -> (programa, 22}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 20 + 17] = {22, -1, &semantic_tbd};

    /* ({programa, 21}  ]) -> (programa, 20}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 21 + 18] = {20, -1, &semantic_tbd};

    /* ({programa, 22}  int) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 22 + 6] = {13, -1, &semantic_tbd};

    /* ({programa, 22}  float) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 22 + 7] = {13, -1, &semantic_tbd};

    /* ({programa, 22}  bool) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 22 + 8] = {13, -1, &semantic_tbd};

    /* ({programa, 22}  char) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 22 + 9] = {13, -1, &semantic_tbd};

    /* ({programa, 22}  string) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 22 + 10] = {13, -1, &semantic_tbd};

    /* ({programa, 22}  identificador) -> (programa, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 22 + 11] = {13, -1, &semantic_tbd};

    /* ({programa, 23}  ;) -> (programa, 24}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 23 + 13] = {24, -1, &semantic_tbd};

    /* ({programa, 24}  identificador) -> (programa, 25}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 24 + 11] = {25, -1, &semantic_tbd};

    /* ({programa, 25}  ;) -> (programa, 26}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 25 + 13] = {26, -1, &semantic_tbd};

    /* ({programa, 26}  end) -> (programa, 18}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 26 + 0] = {18, -1, &semantic_tbd};

    /* ({programa, 26}  identificador) -> (programa, 25}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 26 + 11] = {25, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  int) -> (comando_funcao, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 6] = {28, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  float) -> (comando_funcao, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 7] = {28, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  bool) -> (comando_funcao, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 8] = {28, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  char) -> (comando_funcao, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 9] = {28, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  string) -> (comando_funcao, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 10] = {28, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  identificador) -> (comando_funcao, 2}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 11] = {29, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  if) -> (comando_funcao, 3}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 20] = {30, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  match) -> (comando_funcao, 4}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 21] = {31, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  while) -> (comando_funcao, 5}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 22] = {32, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  repeat) -> (comando_funcao, 6}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 23] = {33, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  for) -> (comando_funcao, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 24] = {34, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  read) -> (comando_funcao, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 25] = {35, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  print) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 26] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  scope) -> (comando_funcao, 10}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 27] = {37, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  =>) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 28] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  break) -> (comando_funcao, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 29] = {38, -1, &semantic_tbd};

    /* ({comando_funcao, 0}  continue) -> (comando_funcao, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 27 + 30] = {38, -1, &semantic_tbd};

    /* ({comando_funcao, 1}  identificador) -> (comando_funcao, 12}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 28 + 11] = {39, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  identificador) -> (comando_funcao, 12}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 11] = {39, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  () -> (comando_funcao, 30}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 15] = {57, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  [) -> (comando_funcao, 28}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 16] = {55, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  =) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 33] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  .) -> (comando_funcao, 29}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 34] = {56, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  /=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 35] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  +=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 36] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  -=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 37] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  *=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 38] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 2}  %=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 29 + 39] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 4}  constante_char) -> (comando_funcao, 41}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 31 + 32] = {68, -1, &semantic_tbd};

    /* ({comando_funcao, 4}  constante_string) -> (comando_funcao, 41}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 31 + 31] = {68, -1, &semantic_tbd};

    /* ({comando_funcao, 6}  ;) -> (comando_funcao, 37}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 33 + 13] = {64, -1, &semantic_tbd};

    /* ({comando_funcao, 7}  identificador) -> (comando_funcao, 31}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 34 + 11] = {58, -1, &semantic_tbd};

    /* ({comando_funcao, 8}  identificador) -> (comando_funcao, 25}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 35 + 11] = {52, -1, &semantic_tbd};

    /* ({comando_funcao, 9}  constante_char) -> (comando_funcao, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 36 + 32] = {38, -1, &semantic_tbd};

    /* ({comando_funcao, 9}  constante_string) -> (comando_funcao, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 36 + 31] = {38, -1, &semantic_tbd};

    /* ({comando_funcao, 10}  ;) -> (comando_funcao, 21}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 37 + 13] = {48, -1, &semantic_tbd};

    /* ({comando_funcao, 11}  ;) -> (comando_funcao, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 38 + 13] = {40, -1, &semantic_tbd};

    /* ({comando_funcao, 12}  ;) -> (comando_funcao, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 39 + 13] = {40, -1, &semantic_tbd};

    /* ({comando_funcao, 12}  [) -> (comando_funcao, 16}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 39 + 16] = {43, -1, &semantic_tbd};

    /* ({comando_funcao, 12}  ,) -> (comando_funcao, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 39 + 17] = {42, -1, &semantic_tbd};

    /* ({comando_funcao, 12}  =) -> (comando_funcao, 14}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 39 + 33] = {41, -1, &semantic_tbd};

    /* ({comando_funcao, 14}  constante_char) -> (comando_funcao, 24}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 41 + 32] = {51, -1, &semantic_tbd};

    /* ({comando_funcao, 14}  constante_string) -> (comando_funcao, 24}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 41 + 31] = {51, -1, &semantic_tbd};

    /* ({comando_funcao, 15}  identificador) -> (comando_funcao, 23}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 42 + 11] = {50, -1, &semantic_tbd};

    /* ({comando_funcao, 16}  numero_inteiro) -> (comando_funcao, 17}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 43 + 19] = {44, -1, &semantic_tbd};

    /* ({comando_funcao, 17}  ]) -> (comando_funcao, 18}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 44 + 18] = {45, -1, &semantic_tbd};

    /* ({comando_funcao, 18}  ,) -> (comando_funcao, 19}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 45 + 17] = {46, -1, &semantic_tbd};

    /* ({comando_funcao, 18}  [) -> (comando_funcao, 16}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 45 + 16] = {43, -1, &semantic_tbd};

    /* ({comando_funcao, 19}  identificador) -> (comando_funcao, 20}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 46 + 11] = {47, -1, &semantic_tbd};

    /* ({comando_funcao, 20}  [) -> (comando_funcao, 16}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 47 + 16] = {43, -1, &semantic_tbd};

    /* ({comando_funcao, 22}  end) -> (comando_funcao, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 49 + 0] = {38, -1, &semantic_tbd};

    /* ({comando_funcao, 23}  ;) -> (comando_funcao, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 50 + 13] = {40, -1, &semantic_tbd};

    /* ({comando_funcao, 23}  ,) -> (comando_funcao, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 50 + 17] = {42, -1, &semantic_tbd};

    /* ({comando_funcao, 23}  =) -> (comando_funcao, 14}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 50 + 33] = {41, -1, &semantic_tbd};

    /* ({comando_funcao, 24}  ;) -> (comando_funcao, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 51 + 13] = {40, -1, &semantic_tbd};

    /* ({comando_funcao, 24}  ,) -> (comando_funcao, 15}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 51 + 17] = {42, -1, &semantic_tbd};

    /* ({comando_funcao, 25}  ;) -> (comando_funcao, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 52 + 13] = {40, -1, &semantic_tbd};

    /* ({comando_funcao, 25}  [) -> (comando_funcao, 26}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 52 + 16] = {53, -1, &semantic_tbd};

    /* ({comando_funcao, 25}  .) -> (comando_funcao, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 52 + 34] = {35, -1, &semantic_tbd};

    /* ({comando_funcao, 27}  ]) -> (comando_funcao, 25}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 54 + 18] = {52, -1, &semantic_tbd};

    /* ({comando_funcao, 29}  identificador) -> (comando_funcao, 39}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 56 + 11] = {66, -1, &semantic_tbd};

    /* ({comando_funcao, 30}  constante_char) -> (comando_funcao, 32}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 57 + 32] = {59, -1, &semantic_tbd};

    /* ({comando_funcao, 30}  constante_string) -> (comando_funcao, 32}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 57 + 31] = {59, -1, &semantic_tbd};

    /* ({comando_funcao, 30}  )) -> (comando_funcao, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 57 + 12] = {38, -1, &semantic_tbd};

    /* ({comando_funcao, 31}  in) -> (comando_funcao, 33}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 58 + 45] = {60, -1, &semantic_tbd};

    /* ({comando_funcao, 32}  )) -> (comando_funcao, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 59 + 12] = {38, -1, &semantic_tbd};

    /* ({comando_funcao, 32}  ,) -> (comando_funcao, 35}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 59 + 17] = {62, -1, &semantic_tbd};

    /* ({comando_funcao, 34}  to) -> (comando_funcao, 5}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 61 + 42] = {32, -1, &semantic_tbd};

    /* ({comando_funcao, 35}  constante_char) -> (comando_funcao, 32}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 62 + 32] = {59, -1, &semantic_tbd};

    /* ({comando_funcao, 35}  constante_string) -> (comando_funcao, 32}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 62 + 31] = {59, -1, &semantic_tbd};

    /* ({comando_funcao, 36}  ;) -> (comando_funcao, 22}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 63 + 13] = {49, -1, &semantic_tbd};

    /* ({comando_funcao, 37}  until) -> (comando_funcao, 38}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 64 + 46] = {65, -1, &semantic_tbd};

    /* ({comando_funcao, 39}  [) -> (comando_funcao, 28}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 66 + 16] = {55, -1, &semantic_tbd};

    /* ({comando_funcao, 39}  =) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 66 + 33] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 39}  .) -> (comando_funcao, 29}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 66 + 34] = {56, -1, &semantic_tbd};

    /* ({comando_funcao, 39}  /=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 66 + 35] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 39}  +=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 66 + 36] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 39}  -=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 66 + 37] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 39}  *=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 66 + 38] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 39}  %=) -> (comando_funcao, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 66 + 39] = {36, -1, &semantic_tbd};

    /* ({comando_funcao, 40}  ]) -> (comando_funcao, 39}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 67 + 18] = {66, -1, &semantic_tbd};

    /* ({comando_funcao, 41}  with) -> (comando_funcao, 42}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 68 + 43] = {69, -1, &semantic_tbd};

    /* ({comando_funcao, 42}  ;) -> (comando_funcao, 43}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 69 + 13] = {70, -1, &semantic_tbd};

    /* ({comando_funcao, 42}  |) -> (comando_funcao, 44}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 69 + 41] = {71, -1, &semantic_tbd};

    /* ({comando_funcao, 43}  |) -> (comando_funcao, 44}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 70 + 41] = {71, -1, &semantic_tbd};

    /* ({comando_funcao, 44}  constante_char) -> (comando_funcao, 45}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 71 + 32] = {72, -1, &semantic_tbd};

    /* ({comando_funcao, 44}  constante_string) -> (comando_funcao, 45}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 71 + 31] = {72, -1, &semantic_tbd};

    /* ({comando_funcao, 45}  :) -> (comando_funcao, 46}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 72 + 40] = {73, -1, &semantic_tbd};

    /* ({comando_funcao, 45}  |) -> (comando_funcao, 44}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 72 + 41] = {71, -1, &semantic_tbd};

    /* ({comando_funcao, 47}  |) -> (comando_funcao, 48}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 74 + 41] = {75, -1, &semantic_tbd};

    /* ({comando_funcao, 47}  ;) -> (comando_funcao, 43}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 74 + 13] = {70, -1, &semantic_tbd};

    /* ({comando_funcao, 47}  end) -> (comando_funcao, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 74 + 0] = {38, -1, &semantic_tbd};

    /* ({comando_funcao, 48}  constante_string) -> (comando_funcao, 45}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 75 + 31] = {72, -1, &semantic_tbd};

    /* ({comando_funcao, 48}  constante_char) -> (comando_funcao, 45}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 75 + 32] = {72, -1, &semantic_tbd};

    /* ({comando_funcao, 48}  else) -> (comando_funcao, 50}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 75 + 44] = {77, -1, &semantic_tbd};

    /* ({comando_funcao, 49}  ;) -> (comando_funcao, 51}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 76 + 13] = {78, -1, &semantic_tbd};

    /* ({comando_funcao, 50}  :) -> (comando_funcao, 21}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 77 + 40] = {48, -1, &semantic_tbd};

    /* ({comando_funcao, 52}  else) -> (comando_funcao, 53}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 79 + 44] = {80, -1, &semantic_tbd};

    /* ({comando_funcao, 52}  end) -> (comando_funcao, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 79 + 0] = {38, -1, &semantic_tbd};

    /* ({comando_funcao, 53}  if) -> (comando_funcao, 3}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 80 + 20] = {30, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 0}  false) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 81 + 56] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 0}  true) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 81 + 55] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 0}  numero_inteiro) -> (expressao_aritmetica, 5}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 81 + 19] = {86, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 0}  numero_float) -> (expressao_aritmetica, 5}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 81 + 53] = {86, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 0}  -) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 81 + 47] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 0}  !) -> (expressao_aritmetica, 6}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 81 + 54] = {87, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 0}  () -> (expressao_aritmetica, 4}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 81 + 15] = {85, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 0}  identificador) -> (expressao_aritmetica, 2}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 81 + 11] = {83, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 1}  false) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 82 + 56] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 1}  true) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 82 + 55] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 1}  numero_inteiro) -> (expressao_aritmetica, 5}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 82 + 19] = {86, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 1}  numero_float) -> (expressao_aritmetica, 5}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 82 + 53] = {86, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 1}  !) -> (expressao_aritmetica, 6}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 82 + 54] = {87, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 1}  () -> (expressao_aritmetica, 4}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 82 + 15] = {85, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 1}  identificador) -> (expressao_aritmetica, 2}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 82 + 11] = {83, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  %) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 51] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  +) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 52] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  /) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 50] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  ^) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 48] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  -) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 47] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  *) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 49] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  ==) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 57] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  |) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 41] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  &) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 59] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  !=) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 58] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  ++) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 60] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  --) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 61] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  .) -> (expressao_aritmetica, 10}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 34] = {91, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  () -> (expressao_aritmetica, 11}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 15] = {92, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 2}  [) -> (expressao_aritmetica, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 83 + 16] = {90, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 3}  ==) -> (expressao_aritmetica, 17}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 84 + 57] = {98, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 3}  !=) -> (expressao_aritmetica, 17}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 84 + 58] = {98, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 3}  >) -> (expressao_aritmetica, 17}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 84 + 62] = {98, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 3}  >=) -> (expressao_aritmetica, 17}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 84 + 63] = {98, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 3}  <) -> (expressao_aritmetica, 17}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 84 + 64] = {98, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 3}  <=) -> (expressao_aritmetica, 17}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 84 + 65] = {98, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 5}  +) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 86 + 52] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 5}  %) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 86 + 51] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 5}  /) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 86 + 50] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 5}  *) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 86 + 49] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 5}  ^) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 86 + 48] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 5}  -) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 86 + 47] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 6}  identificador) -> (expressao_aritmetica, 2}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 87 + 11] = {83, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 6}  () -> (expressao_aritmetica, 4}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 87 + 15] = {85, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 6}  true) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 87 + 55] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 6}  false) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 87 + 56] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  +) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 52] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  -) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 47] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  ^) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 48] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  *) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 49] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  %) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 51] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  /) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 50] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  &) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 59] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  !=) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 58] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  ==) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 57] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 7}  |) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 88 + 41] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 8}  false) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 89 + 56] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 8}  true) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 89 + 55] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 8}  !) -> (expressao_aritmetica, 6}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 89 + 54] = {87, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 8}  () -> (expressao_aritmetica, 4}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 89 + 15] = {85, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 8}  identificador) -> (expressao_aritmetica, 2}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 89 + 11] = {83, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 10}  identificador) -> (expressao_aritmetica, 14}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 91 + 11] = {95, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 11}  constante_char) -> (expressao_aritmetica, 12}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 92 + 32] = {93, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 11}  constante_string) -> (expressao_aritmetica, 12}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 92 + 31] = {93, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 11}  )) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 92 + 12] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 12}  )) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 93 + 12] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 12}  ,) -> (expressao_aritmetica, 13}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 93 + 17] = {94, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 13}  constante_char) -> (expressao_aritmetica, 12}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 94 + 32] = {93, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 13}  constante_string) -> (expressao_aritmetica, 12}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 94 + 31] = {93, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  +) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 52] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  %) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 51] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  /) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 50] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  ^) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 48] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  -) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 47] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  *) -> (expressao_aritmetica, 1}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 49] = {82, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  !=) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 58] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  ==) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 57] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  &) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 59] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  |) -> (expressao_aritmetica, 8}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 41] = {89, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  --) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 61] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  ++) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 60] = {88, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  .) -> (expressao_aritmetica, 10}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 34] = {91, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 14}  [) -> (expressao_aritmetica, 9}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 95 + 16] = {90, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 15}  ]) -> (expressao_aritmetica, 14}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 96 + 18] = {95, -1, &semantic_tbd};

    /* ({expressao_aritmetica, 16}  )) -> (expressao_aritmetica, 7}, NAO EMPILHA */
    machine->transitionMatrix[nSPAColumns * 97 + 12] = {88, -1, &semantic_tbd};

    for(i = 0; i < 66; i++)
    {
        /* ({expressao_aritmetica, 17}) -> {expressao_aritmetica, 0}, VOLTA PARA {expressao_aritmetica, 7} */
        machine->transitionMatrix[nSPAColumns * 98 + i] = {81, 88, &semantic_tbd};

        /* ({programa, 5}) -> DESEMPILHA */
        machine->transitionMatrix[nSPAColumns * 5 + i] = {-1, -1, &final_semantic_tbd};

        /* ({comando_funcao, 51}) -> {comando_funcao, 0}, VOLTA PARA {comando_funcao, 52} */
        machine->transitionMatrix[nSPAColumns * 78 + i] = {27, 79, &semantic_tbd};

        /* ({comando_funcao, 3}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 49} */
        machine->transitionMatrix[nSPAColumns * 30 + i] = {81, 76, &semantic_tbd};

        /* ({expressao_aritmetica, 4}) -> {expressao_aritmetica, 0}, VOLTA PARA {expressao_aritmetica, 16} */
        machine->transitionMatrix[nSPAColumns * 85 + i] = {81, 97, &semantic_tbd};

        /* ({comando_funcao, 5}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 36} */
        machine->transitionMatrix[nSPAColumns * 32 + i] = {81, 63, &semantic_tbd};

        /* ({expressao_aritmetica, 9}) -> {expressao_aritmetica, 0}, VOLTA PARA {expressao_aritmetica, 15} */
        machine->transitionMatrix[nSPAColumns * 90 + i] = {81, 96, &semantic_tbd};

        /* ({comando_funcao, 13}) -> DESEMPILHA */
        machine->transitionMatrix[nSPAColumns * 40 + i] = {-1, -1, &final_semantic_tbd};

        /* ({comando_funcao, 46}) -> {comando_funcao, 0}, VOLTA PARA {comando_funcao, 47} */
        machine->transitionMatrix[nSPAColumns * 73 + i] = {27, 74, &semantic_tbd};

        /* ({comando_funcao, 38}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 11} */
        machine->transitionMatrix[nSPAColumns * 65 + i] = {81, 38, &semantic_tbd};

        /* ({comando_funcao, 21}) -> {comando_funcao, 0}, VOLTA PARA {comando_funcao, 22} */
        machine->transitionMatrix[nSPAColumns * 48 + i] = {27, 49, &semantic_tbd};

        /* ({comando_funcao, 33}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 34} */
        machine->transitionMatrix[nSPAColumns * 60 + i] = {81, 61, &semantic_tbd};

        /* ({comando_funcao, 28}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 40} */
        machine->transitionMatrix[nSPAColumns * 55 + i] = {81, 67, &semantic_tbd};

        /* ({comando_funcao, 26}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 27} */
        machine->transitionMatrix[nSPAColumns * 53 + i] = {81, 54, &semantic_tbd};

        /* ({comando_funcao, 53}) -> {comando_funcao, 0}, VOLTA PARA {comando_funcao, 22} */
        if (i != 20)
            machine->transitionMatrix[nSPAColumns * 80 + i] = {27, 49, &semantic_tbd};

        /* ({comando_funcao, 22}) -> {comando_funcao, 0}, VOLTA PARA {comando_funcao, 22} */
        if (i != 0)
            machine->transitionMatrix[nSPAColumns * 49 + i] = {27, 49, &semantic_tbd};

        /* ({programa, 16}) -> {comando_funcao, 0}, VOLTA PARA {programa, 16} */
        if (i != 0)
            machine->transitionMatrix[nSPAColumns * 16 + i] = {27, 16, &semantic_tbd};

        /* ({comando_funcao, 37}) -> {comando_funcao, 0}, VOLTA PARA {comando_funcao, 37} */
        if (i != 46)
            machine->transitionMatrix[nSPAColumns * 64 + i] = {27, 64, &semantic_tbd};

        /* ({expressao_aritmetica, 13}) -> {expressao_aritmetica, 0}, VOLTA PARA {expressao_aritmetica, 12} */
        if (i != 32 && i != 31)
            machine->transitionMatrix[nSPAColumns * 94 + i] = {81, 93, &semantic_tbd};

        /* ({comando_funcao, 18}) -> DESEMPILHA */
        if (i != 17 && i != 16)
            machine->transitionMatrix[nSPAColumns * 45 + i] = {-1, -1, &final_semantic_tbd};

        /* ({comando_funcao, 14}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 24} */
        if (i != 32 && i != 31)
            machine->transitionMatrix[nSPAColumns * 41 + i] = {81, 51, &semantic_tbd};

        /* ({comando_funcao, 9}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 11} */
        if (i != 32 && i != 31)
            machine->transitionMatrix[nSPAColumns * 36 + i] = {81, 38, &semantic_tbd};

        /* ({comando_funcao, 4}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 41} */
        if (i != 32 && i != 31)
            machine->transitionMatrix[nSPAColumns * 31 + i] = {81, 68, &semantic_tbd};

        /* ({comando_funcao, 35}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 32} */
        if (i != 32 && i != 31)
            machine->transitionMatrix[nSPAColumns * 62 + i] = {81, 59, &semantic_tbd};

        /* ({comando_funcao, 52}) -> {comando_funcao, 0}, VOLTA PARA {comando_funcao, 52} */
        if (i != 44 && i != 0)
            machine->transitionMatrix[nSPAColumns * 79 + i] = {27, 79, &semantic_tbd};

        /* ({comando_funcao, 44}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 45} */
        if (i != 32 && i != 31)
            machine->transitionMatrix[nSPAColumns * 71 + i] = {81, 72, &semantic_tbd};

        /* ({comando_funcao, 47}) -> {comando_funcao, 0}, VOLTA PARA {comando_funcao, 47} */
        if (i != 41 && i != 13 && i != 0)
            machine->transitionMatrix[nSPAColumns * 74 + i] = {27, 74, &semantic_tbd};

        /* ({comando_funcao, 30}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 32} */
        if (i != 32 && i != 31 && i != 12)
            machine->transitionMatrix[nSPAColumns * 57 + i] = {81, 59, &semantic_tbd};

        /* ({comando_funcao, 48}) -> {expressao_aritmetica, 0}, VOLTA PARA {comando_funcao, 45} */
        if (i != 31 && i != 32 && i != 44)
            machine->transitionMatrix[nSPAColumns * 75 + i] = {81, 72, &semantic_tbd};

        /* ({expressao_aritmetica, 11}) -> {expressao_aritmetica, 0}, VOLTA PARA {expressao_aritmetica, 12} */
        if (i != 32 && i != 31 && i != 12)
            machine->transitionMatrix[nSPAColumns * 92 + i] = {81, 93, &semantic_tbd};

        /* ({programa, 0}) -> {comando_funcao, 0}, VOLTA PARA {programa, 0} */
        if (i != 5 && i != 4 && i != 3 && i != 2 && i != 1)
            machine->transitionMatrix[nSPAColumns * 0 + i] = {27, 0, &semantic_tbd};

        /* ({expressao_aritmetica, 8}) -> {expressao_aritmetica, 0}, VOLTA PARA {expressao_aritmetica, 3} */
        if (i != 56 && i != 55 && i != 54 && i != 15 && i != 11)
            machine->transitionMatrix[nSPAColumns * 89 + i] = {81, 84, &semantic_tbd};

        /* ({expressao_aritmetica, 5}) -> DESEMPILHA */
        if (i != 52 && i != 51 && i != 50 && i != 49 && i != 48 && i != 47)
            machine->transitionMatrix[nSPAColumns * 86 + i] = {-1, -1, &final_semantic_tbd};

        /* ({expressao_aritmetica, 1}) -> {expressao_aritmetica, 0}, VOLTA PARA {expressao_aritmetica, 3} */
        if (i != 56 && i != 55 && i != 19 && i != 53 && i != 54 && i != 15 && i != 11)
            machine->transitionMatrix[nSPAColumns * 82 + i] = {81, 84, &semantic_tbd};

        /* ({expressao_aritmetica, 0}) -> {expressao_aritmetica, 0}, VOLTA PARA {expressao_aritmetica, 3} */
        if (i != 56 && i != 55 && i != 19 && i != 53 && i != 47 && i != 54 && i != 15 && i != 11)
            machine->transitionMatrix[nSPAColumns * 81 + i] = {81, 84, &semantic_tbd};

        /* ({expressao_aritmetica, 7}) -> DESEMPILHA */
        if (i != 52 && i != 47 && i != 48 && i != 49 && i != 51 && i != 50 && i != 59 && i != 58 && i != 57 && i != 41)
            machine->transitionMatrix[nSPAColumns * 88 + i] = {-1, -1, &final_semantic_tbd};

        /* ({expressao_aritmetica, 14}) -> DESEMPILHA */
        if (i != 52 && i != 51 && i != 50 && i != 48 && i != 47 && i != 49 && i != 58 && i != 57 && i != 59 && i != 41 && i != 61 && i != 60 && i != 34 && i != 16)
            machine->transitionMatrix[nSPAColumns * 95 + i] = {-1, -1, &final_semantic_tbd};

        /* ({expressao_aritmetica, 2}) -> DESEMPILHA */
        if (i != 51 && i != 52 && i != 50 && i != 48 && i != 47 && i != 49 && i != 57 && i != 41 && i != 59 && i != 58 && i != 60 && i != 61 && i != 34 && i != 15 && i != 16)
            machine->transitionMatrix[nSPAColumns * 83 + i] = {-1, -1, &final_semantic_tbd};

    }


    return machine;

}


