require 'Mechanize'
require './output_builder'

module Syntactic
  
  class Scraper
    
    attr_accessor :columns, :final_states, :num_states, :transitions, :sub_machines, :sub_machines_offsets, :states, :columns_by_index
    
    def initialize
      @agent = Mechanize.new
      @columns = {}
      @final_states = []
      @num_states = 0
      @transitions = []
      @sub_machines_offsets = {}
      @sub_machines = []
      @states = {}
      @columns_by_index = {}
    end
    
    def run
      get_page
      scrape_page
      # scrape_page(get_page)
      Syntactic::OutputBuilder.new(self).generate_output
    end
    
    
    def get_page
      
      # A gramática deve estar em um arquivo wirth.txt no mesmo diretório do script
      
      begin
        wirth_contents = File.open('wirth.txt', 'r') { |f| f.read } 
      rescue Exception => ex
        puts "Couldn't open file wirth.txt. Make sure the file exists and try again."
        exit
      end
      
      @agent.get 'http://radiant-fire-72.heroku.com'
      @agent.page.forms.first.fields_with(name: 'wirth_notation').first.value = wirth_contents
      
      # Número de tentativas de enviar o gramática, porque às vezes falha.
      num_tries = 0
      
      begin
        if num_tries < 3
          puts "Try ##{num_tries}"
          @agent.page.forms.first.submit
        else
          puts "Couldn't submit form."
        end
      rescue Exception => ex
        num_tries += 1
        retry
      end
      
      # Salva a resposta. Debug only.
      File.open('result.html', 'w') {|f| f.write(@agent.page.body)}      
      
      # return Nokogiri::HTML(File.open('result.html', 'r'){|f| f.read})
      
    end
    
    def scrape_page(doc=nil)
      
      unless doc
        doc = @agent.page.parser
      end

      # Número da próxima coluna
      col_number = 0
      
      # Número do próximo estado
      row_number = 0
      
      # Offset da máquina atual (para calcular o estado)
      row_offset = 0

      # Salva o nome de todas as sub-máquinas
      doc.css('h3').each do |node|
        sub_machines << node.text
      end
      
      # Para cada submáquina...
      doc.css('h3').each do |node|

        sub_machine_name = node.text

        # puts "##{sub_machine_name}: #{row_offset}"
        
        @sub_machines_offsets[sub_machine_name] = row_offset

        # XML do JFLAP
        
        xml_text = node.next_element.xpath('div/pre').last.text
              
        File.open('test.txt', 'a') {|f| f.write(xml_text)}
               
        xml = Nokogiri::HTML(xml_text)
        
        #puts xml

        # Número de estados nesta submáquina
        sub_machine_state_count = xml.xpath('//state').count
        
        @num_states += sub_machine_state_count

        # Marcação dos estados finais da submáquina
        xml.xpath('//state').each do |state|
          unless state.xpath('final').empty?
            @final_states << (row_offset + state.attr('id').to_i)
          end
        end

        xml.xpath('//transition').each do |transition|

          read = transition.xpath('read').text

          terminal = false # Assume que é outra submáquina
          col_symbol = read

          if read.start_with? "\"" # É palavra reservada ou caractere especial
            col_symbol = read.match(/"(.*)"/).captures.first
            terminal = true
          elsif !@sub_machines.include?(read) # É identificador, número inteiro, etc.
            terminal = true
          end
          
          # Cria uma nova coluna para o terminal
          if terminal && !@columns[col_symbol]
            @columns[col_symbol] = col_number
            @columns_by_index[col_number] = col_symbol
            col_number += 1
          end
          
          from = transition.xpath('from').first.text.to_i
          to = transition.xpath('to').first.text.to_i
          
          unless @states[row_offset + from]
            @states[row_offset + from] = sub_machine_name
          end
          
          unless @states[row_offset + to]
            @states[row_offset + to] = sub_machine_name
          end
          
          @transitions << { :terminal => terminal, :column => col_symbol, :from => row_offset + from, :to => row_offset + to, :submachine => sub_machine_name, :original_from => from, :original_to => to}

        end

        row_offset += sub_machine_state_count

      end

      # Ordena o conjunto de transições
      
      @transitions.sort! do |a, b|
        
        if a[:submachine] == b[:submachine]
          a1 = a[:terminal] ? 1 : 0
          b1 = b[:terminal] ? 1 : 0  
          a1 <=> b1
        else
          a[:submachine] <=> b[:submachine]
        end
        
      end

    end
    
  end
  
end