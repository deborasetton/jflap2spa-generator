/*******************************************************
    ATENÇÃO: ESTE ARQUIVO FOI GERADO AUTOMATICAMENTE.
*******************************************************/
    
#include "syntactic_generated.h"
#include "spstatemachine.h"
#include "semantic.h"

#IS_FINAL_ARRAY#

#RESERVED_WORDS_MAP#

int IsFinal(int state)
{
    return _isFinal[state];
}

int GetColumn(Token token)
{
  
#GET_COLUMN_BODY#
  
}

SPStateMachine * InitializeSPA()
{
    SPStateMachine *machine;   
    int nSPAColumns = #COLUMN_COUNT#;
    SPTransition null_transition = {-1, -1, NULL};

    machine = SPCreateStateMachine(#LINE_COUNT#, #COLUMN_COUNT#, null_transition);

    /* SPA transitions */

#STATE_MACHINE_ASSIGNMENTS#

    return machine;

}


